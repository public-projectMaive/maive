using api.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Builder;
using api.Interfaces;
using api.Repository;
using api.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using api.Service;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddControllers().AddNewtonsoftJson(options => {
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
}); 

builder.Services.AddDbContext<ApplicationDBContext>(options => 
{

    var serverVersion = new MySqlServerVersion(new Version(10,5,23));

    // gets the ConnectionsString from appsettings.json 
    options.UseMySql(builder.Configuration.GetConnectionString("DefaultConnection"), serverVersion);
});

// Identity Settings  
// Adds identity services to the DI container. 'AppUser' and 'IdentityRole' specify the types for the user and the role.
builder.Services.AddIdentity<AppUser, IdentityRole>(options => {
    // Configures the requirements for user passwords.
    options.Password.RequireDigit = true; // Passwords must contain a digit.
    options.Password.RequireLowercase = true; // Passwords must contain a lowercase letter.
    options.Password.RequireUppercase = true; // Passwords must contain an uppercase letter.
    options.Password.RequireNonAlphanumeric = true; // Passwords must contain a non-alphanumeric character.
    options.Password.RequiredLength = 12; // Passwords must be at least 12 characters long.
})
// Links Identity to Entity Framework, using 'ApplicationDBContext' as the database context.
.AddEntityFrameworkStores<ApplicationDBContext>();

// Adds authentication services and configures default schemes for various authentication purposes.
builder.Services.AddAuthentication(options => {
    // Configures all default schemes to 'JwtBearerDefaults.AuthenticationScheme', i.e., JWT Bearer Token.
    options.DefaultAuthenticateScheme = 
    options.DefaultChallengeScheme = 
    options.DefaultForbidScheme = 
    options.DefaultScheme = 
    options.DefaultSignInScheme = 
    options.DefaultSignOutScheme = JwtBearerDefaults.AuthenticationScheme; // Sets the default scheme to JWT.
})
// Adds support for JWT Bearer Tokens.
.AddJwtBearer(options => {
    options.TokenValidationParameters = new TokenValidationParameters{
        ValidateIssuer = true, // Validates the issuer of the token.
        ValidIssuer = builder.Configuration["JWT:Issuer"], // Sets the valid issuer of the token from the configuration.
        ValidateAudience = true, // Validates the audience of the token.
        ValidAudience = builder.Configuration["JWT:Audience"], // Sets the valid audience of the token from the configuration.
        ValidateIssuerSigningKey = true, // Validates the key used to sign the token.
        IssuerSigningKey = new SymmetricSecurityKey(
            System.Text.Encoding.UTF8.GetBytes(builder.Configuration["JWT:SigningKey"])
        ) // Creates the security key from the key specified in the configuration.
    };
});


builder.Services.AddScoped<ICompanyRepository, CompanyRepository>(); 
builder.Services.AddScoped<ISdgTargetRepository, SdgTargetRepository>(); 
builder.Services.AddScoped<ITokenService, TokenService>(); 
builder.Services.AddScoped<ISdgTargetSubGoalRepository, SdgTargetSubGoalRepository>(); 
builder.Services.AddScoped<ISdgTargetSdgTypeRepository, SdgTargetSdgTypeRepository>(); 
builder.Services.AddScoped<ISdgTypeRepository, SdgTypeRepository>(); 






builder.Services.AddCors(options =>
{
    options.AddPolicy("MyCorsPolicy", builder =>
    {
        builder.WithOrigins("http://localhost:5173")
                .AllowAnyHeader()
                .AllowAnyMethod();
    });
});




var app = builder.Build();

// Seed the database with initial data
using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    var configuration = services.GetRequiredService<IConfiguration>();
    await DbInitializer.SeedAdminUserAsync(services, configuration);
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("MyCorsPolicy");


app.UseHttpsRedirection();
app.UseAuthentication(); 
app.UseAuthorization(); 
// without that swagger will not work? 
app.MapControllers(); 

app.Run();


