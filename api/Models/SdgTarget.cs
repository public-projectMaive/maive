using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace api.Models
{
    // The 'Table' attribute specifies the database table that this class maps to.
    // 'SdgTargets' is the name of the table in the database.
    [Table("SdgTargets")]
    public class SdgTarget
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty; 
        public string Description { get; set; } = string.Empty;
        public bool IsDone { get; set; }
        
        // 'CreatedOn' property: A DateTime to record when the SdgTarget was created. It defaults to the current date and time.
        public DateTime CreationDate { get; set; } = DateTime.Now; 

        // 'CompanyId' property: An integer that serves as a foreign key linking this SdgTarget to a Company.
        // It is nullable (int?) indicating that a comment might not be associated with a Company.
        //***** sollte noch geändert werden dass es null sein kann
        // ******** kann eigentlich nicht null gucken wie man das abfangen kann 
        public int? CompanyId { get; set; }
        // 'Company' property: This is a navigation property. Navigation properties allow your Entity Framework
        // models to include references to other entities, facilitating the traversal of related entities in your models.
        // For instance, this property allows you to access the 'Stock' entity related to this 'Company' directly.
        public Company? Company { get; set; }

        public List<SdgTargetSubGoal> SubGoals { get; set; } = new List<SdgTargetSubGoal>(); 
        public List<SdgTargetSdgType> SdgTargetSdgTypes { get; set; } = new List<SdgTargetSdgType>();
    }
}