using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using api.Dtos.Company;
using api.Models;

namespace api.Mappers
{
    public static class CompanyMapper
    {
        public static CompanyDto ToCompanyDto(this Company companyModel){
            return new CompanyDto{
                Id = companyModel.Id,
                Name = companyModel.Name,
                
                Street = companyModel.Street,
                StreetNumber = companyModel.StreetNumber,
                Postalcode = companyModel.Postalcode,
                City = companyModel.City,
                Latitude = companyModel.Latitude,
                Longitude = companyModel.Longitude,
                // ************ nochmal gucken ob die wirklich gebraucht werden 
                SdgTargets = companyModel.SdgTargets,
                AppUsers = companyModel.AppUsers
            };
        }
        public static Company ToCompanyFromCreateDTO(this CreateCompanyDto companyDto){
            return  new Company{
                
                Name = companyDto.Name,
                
                Street = companyDto.Street,
                StreetNumber = companyDto.StreetNumber,
                Postalcode = companyDto.Postalcode,
                City = companyDto.City,
                Latitude = companyDto.Latitude,
                Longitude = companyDto.Longitude,
            };
        }
    }
}