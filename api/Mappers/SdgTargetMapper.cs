using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Dtos.SdgTargetDtos;
using api.Models;
using Mysqlx.Crud;

namespace api.Mappers
{
    public static class SdgTargetMapper
    {
        // this wird verwendet, um eine Erweiterungsmethode zu definieren 
        //
        public static SdgTargetDto ToSdgTargetDto(this SdgTarget sdgTargetModel ){
            if (sdgTargetModel == null) 
                throw new ArgumentNullException(nameof(sdgTargetModel));

            return new SdgTargetDto
            {
                Id = sdgTargetModel.Id,
                Title = sdgTargetModel.Title,
                Description = sdgTargetModel.Description,
                IsDone = sdgTargetModel.IsDone,
                SubGoals = sdgTargetModel.SubGoals.Select(s => s.ToSdgTargetSubGoalDto()).ToList(),
                SdgTypeColors = sdgTargetModel.SdgTargetSdgTypes?
                    .Where(st => st.SdgType != null) // Ensure SdgType is not null
                    .Select(st => st.SdgType.Color)
                    .ToList() ?? new List<string>() // Default to empty list if null
            };
        }

        public static SdgTarget ToSdgTargetFromCreate(this CreateSdgTargetDto createSdgTargetDto){
            return new SdgTarget{
                Title = createSdgTargetDto.Title,
                Description = createSdgTargetDto.Description,
                IsDone = createSdgTargetDto.IsDone,
                
            };
        }
        public static SdgTarget ToSdgTargetFromUpdate(this UpdateSdgTargetRequestDto updateSdgTargetRequestDto){
            return new SdgTarget{
                Title = updateSdgTargetRequestDto.Title,
                Description = updateSdgTargetRequestDto.Description,
                IsDone = updateSdgTargetRequestDto.IsDone
            };
        }
    }
}