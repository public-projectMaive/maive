using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Data;
using api.Interfaces;
using api.Models;

namespace api.Repository
{
    public class SdgTypeRepository : ISdgTypeRepository
    {
        // Field to hold the database context used for data operations.
        private readonly ApplicationDBContext _context; 

        // Constructor that injects the database context into the repository.
        // This allows the repository to perform data operations.
        public SdgTypeRepository(ApplicationDBContext context)
        {
            _context = context; 
        }
    
        public Task<SdgType> CreateAsync(SdgType sdgModel)
        {
            throw new NotImplementedException();
        }

        public Task<List<SdgType>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<SdgType?> GetByIdAsync(int id)
        {
            return await _context.SdgTypes.FindAsync(id);        
        }
    }
}