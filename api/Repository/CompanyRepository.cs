using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Data;
using api.Dtos.Company;
using api.Interfaces;
using api.Models;
using Microsoft.EntityFrameworkCore;

namespace api.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        // Field to hold the database context used for data operations.
        private readonly ApplicationDBContext _context; 

        // Constructor that injects the database context into the repository.
        // This allows the repository to perform data operations.
        public CompanyRepository(ApplicationDBContext context){
            _context = context; 
        }
        
        public async Task<Company> CreateAsync(Company companyModel)
        {
            //adds a new company to the database.            
            await _context.Companies.AddAsync(companyModel); 
            // After adding, it saves the changes to the database. (necessary)
            await _context.SaveChangesAsync(); 
            // Returns the newly created company.
            return companyModel; 
        }

        public async Task<Company?> DeleteAsync(int id)
        {
            var companyModel = await _context.Companies.FirstOrDefaultAsync(x => x.Id == id ); 
            if (companyModel == null){
                return null; 
            }
            _context.Companies.Remove(companyModel); 
            await _context.SaveChangesAsync(); 
            return companyModel; 
        }

        public async Task<List<Company>> GetAllAsync()
        {   
            // Versuch einen Filter zu bauen - noch mal gucken Query Objekt muss dann als Parameter übergeben werden 
            // var companies = _context.Companies.Include(c => c.Comments).AsQueryable();
            // if(!string.IsNullOrWhiteSpace(query.CompanyName)){
            //     companies = companies.Where(c => c.CompanyName);
            // }
            return await _context.Companies.Include(st => st.SdgTargets).ToListAsync(); 
        }

        public async Task<Company?> GetByIdAsync(int id)
        {
            return await _context.Companies.FindAsync(id); 
        }

        public async Task<Company?> UpdateAsync(int id, UpdateCompanyRequestDto updateCompanyRequestDto)
        {
            var exitstingCompany = await _context.Companies.FindAsync(id); 

            if(exitstingCompany == null){
                return null; 
            }

            exitstingCompany.Name = updateCompanyRequestDto.Name; 

            // Aktuallisierung der Adresse
            exitstingCompany.Street = updateCompanyRequestDto.Street;
            exitstingCompany.StreetNumber = updateCompanyRequestDto.StreetNumber;
            exitstingCompany.Postalcode = updateCompanyRequestDto.Postalcode; 
            exitstingCompany.City = updateCompanyRequestDto.City;
            exitstingCompany.Latitude = updateCompanyRequestDto.Latitude;
            exitstingCompany.Longitude = updateCompanyRequestDto.Longitude;

            await _context.SaveChangesAsync(); 
            return exitstingCompany;
        }
    }
}