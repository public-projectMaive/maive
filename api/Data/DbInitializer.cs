using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using api.Models;

namespace api.Data
{
    public static class DbInitializer
    {
        public static async Task SeedAdminUserAsync(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<AppUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var context = serviceProvider.GetRequiredService<ApplicationDBContext>();

            string adminUserName = configuration["AdminUser:UserName"];
            string adminEmail = configuration["AdminUser:Email"];
            string adminPassword = configuration["AdminUser:Password"];
            string adminRole = configuration["AdminUser:Role"];

            //Prüfen und erstellen der Rolle

            if (!await roleManager.RoleExistsAsync(adminRole))
            {
                await roleManager.CreateAsync(new IdentityRole(adminRole));
            }

            // Prüfen und erstellen des Admin-Users
            if (await userManager.FindByNameAsync(adminUserName) == null)
            {
                var adminUser = new AppUser { UserName = adminUserName, Email = adminEmail, EmailConfirmed = true };
                var result = await userManager.CreateAsync(adminUser, adminPassword);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(adminUser, adminRole);
                }
            }

            // Check and initialize SDG Types 
            if (!context.SdgTypes.Any())
                {
                    var sdgTypes = new[]
                    {
                        new SdgType { Title = "No Poverty", Color = "hsl(0, 81%, 52%)" },
                        new SdgType { Title = "Zero Hunger", Color = "hsl(41, 69%, 55%)" },
                        new SdgType { Title = "Good Health and Well-being", Color = "hsl(134, 47%, 42%)" },
                        new SdgType { Title = "Quality Education", Color = "hsl(0, 81%, 39%)" },
                        new SdgType { Title = "Gender Equality", Color = "hsl(9, 100%, 56%)" },
                        new SdgType { Title = "Clean Water and Sanitation", Color = "hsl(192, 79%, 52%)" },
                        new SdgType { Title = "Affordable and Clean Energy", Color = "hsl(48, 96%, 58%)" },
                        new SdgType { Title = "Decent Work and Economic Growth", Color = "hsl(339, 68%, 39%)" },
                        new SdgType { Title = "Industry, Innovation and Infrastructure", Color = "hsl(18, 100%, 59%)" },
                        new SdgType { Title = "Reduced Inequality", Color = "hsl(327, 81%, 42%)" },
                        new SdgType { Title = "Sustainable Cities and Communities", Color = "hsl(29, 98%, 56%)" },
                        new SdgType { Title = "Responsible Consumption and Production", Color = "hsl(35, 54%, 47%)" },
                        new SdgType { Title = "Climate Action", Color = "hsl(123, 35%, 39%)" },
                        new SdgType { Title = "Life Below Water", Color = "hsl(202, 91%, 43%)" },
                        new SdgType { Title = "Life on Land", Color = "hsl(96, 65%, 46%)" },
                        new SdgType { Title = "Peace and Justice Strong Institutions", Color = "hsl(203, 100%, 31%)" },
                    };

                    context.SdgTypes.AddRange(sdgTypes);
                    await context.SaveChangesAsync();
                }

        }
    }
}