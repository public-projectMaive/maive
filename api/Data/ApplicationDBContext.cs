using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace api.Data
{
    // The ApplicationDBContext class serves as the primary class that coordinates Entity Framework functionality
    // for a given data model, integrating ASP.NET Core Identity to manage users, roles, and other identity features.
    public class ApplicationDBContext : IdentityDbContext<AppUser>
    {
        // base gibt in diesem Fall die Daten weiter in das Interface von dem geerbt wird 
        //Konstruktor
        public ApplicationDBContext(DbContextOptions dbContextOptions)
            : base(dbContextOptions)
        {
            
        }

        // DbSet properties represent collections of entities that correspond to tables in the database.
        // Defining DbSet for SdgTargets and Companies allows the context to manage these entities, including
        // performing CRUD operations. This setup enables the use of LINQ queries on these entities, which
        // are automatically translated into queries against the database.
        public DbSet<SdgTarget> SdgTargets{ get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<SdgTargetSubGoal> SdgTargetSubGoals {get; set;}
        public DbSet<SdgType> SdgTypes {get; set;}
        public DbSet<SdgTargetSdgType> SdgTargetSdgTypes {get; set;}
        

        // is used to make specific configurations and insert standard data, such as user roles, into the database to ensure security from the outset 
        //and simplify the setup process after the initial installation.
        protected override void OnModelCreating(ModelBuilder builder){
            base.OnModelCreating(builder); 
            builder.Entity<SdgTargetSdgType>(x => x.HasKey(s => new{ s.SdgTargetId, s.SdgTypeId}));

            builder.Entity<SdgTargetSdgType>()
                .HasOne(s => s.SdgTarget)
                .WithMany(s => s.SdgTargetSdgTypes)
                .HasForeignKey(s => s.SdgTargetId);
            
            builder.Entity<SdgTargetSdgType>()
                .HasOne(s => s.SdgType)
                .WithMany(s => s.SdgTargetSdgTypes)
                .HasForeignKey(s => s.SdgTypeId);
            
            // This section initializes a list of essential identity roles such as "Admin" and "User". 
            // to ensure necessary access controls are in place from launch.

            List<IdentityRole> roles = new List<IdentityRole>{
                new IdentityRole{
                    Name = "Admin",
                    NormalizedName = "ADMIN" // Uppercase format to standardize role checks across the application.
                }, 
                new IdentityRole{
                    Name = "User",
                    NormalizedName = "USER" // Uppercase format to standardize role checks across the application.
                }, 

            }; 
            // Seeds the roles into the database to ensure they are available immediately after deployment,
            builder.Entity<IdentityRole>().HasData(roles); 
        }
        
    }
}