using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Dtos.Account;
using api.Interfaces;
using api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace api.Controllers
{
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
         // UserManager to handle user-related operations such as finding users, checking passwords, etc.
        private readonly UserManager<AppUser> _userManager;
        // Service for generating JWT tokens.
        private readonly ITokenService _tokenService;
        // SignInManager to handle sign-in related operations like password checks.
        private readonly SignInManager<AppUser> _signInManager;

        // Constructor injects the necessary services.
        public AccountController(UserManager<AppUser> userManager, ITokenService tokenService, SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _tokenService = tokenService; 
            _signInManager = signInManager; 
        }

        
        [HttpPost("login")]  // Route for the login endpoint.
        // This action handles user login attempts.
        public async Task<IActionResult> Login(LoginDto loginDto)
        {
            // Validates model state (data annotations in LoginDto).
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState); 
            }
            
            // Attempts to find a user by username. Converts username to lower case to ensure case-insensitive comparison.
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.UserName == loginDto.Username.ToLower());

            // If no user is found, returns an Unauthorized response.
            if (user == null)
            {
                return Unauthorized("Invalid Username"); 
            }
            // Checks if the password is correct for the found user.
            var result = await _signInManager.CheckPasswordSignInAsync(user, loginDto.Password, false); 

            // If the password is incorrect, returns an Unauthorized response.
            if (!result.Succeeded)
            {
                return Unauthorized("Username not found and/or password incorrect"); 
            }
            // If successful, returns the user data including a token.
            return Ok(new NewUserDto
            {
                Username = user.UserName,
                Email = user.Email,
                Token = _tokenService.CreateToken(user)
            });
        }

        [HttpPost("register")]  // Route for the registration endpoint.
        // This action handles new user registration.
        public async Task<IActionResult> Register([FromBody] RegisterDto registerDto)
        {
            try
            {
                // Validates model state.
                if (!ModelState.IsValid)
                    return BadRequest(ModelState); 
                
                // Creates a new user object.
                var appUser = new AppUser
                {
                    UserName = registerDto.Username,
                    Email = registerDto.Email
                }; 
                
                // Attempts to create the user with the specified password.
                var createdUser = await _userManager.CreateAsync(appUser, registerDto.Password); 

                // If user creation is successful, tries to add the user to the "User" role.
                if (createdUser.Succeeded)
                {
                    var roleResult = await _userManager.AddToRoleAsync(appUser, "User"); 
                    if (roleResult.Succeeded)
                    {
                        // If role assignment is successful, returns the new user data including a token.
                        return Ok(new NewUserDto
                        {
                            Username = appUser.UserName, 
                            Email = appUser.Email, 
                            Token = _tokenService.CreateToken(appUser)
                        }); 
                    }
                    else
                    {
                        // If role assignment fails, returns a server error with the error details.
                        return StatusCode(500, roleResult.Errors); 
                    }
                }
                else
                {
                    // If user creation fails, returns a server error with the error details.
                    return StatusCode(500, createdUser.Errors); 
                }
            }
            catch (Exception e)
            {
                // Catches any exceptions during the process and returns a server error with the exception details.
                return StatusCode(500, e); 
            }
        }
    }
}