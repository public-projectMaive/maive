using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Interfaces;
using api.Mappers;
using api.Models;
using Microsoft.AspNetCore.Mvc;
using api.Dtos.SdgTargetDtos;
using api.Dtos.SdgTargetSubGoalDtos;
using api.Helpers;


namespace api.Controllers 
{   
    // Route attribute defining the base access path for all actions in this controller.
    [Route("api/sdg")]
    // Indicates that this class functions as a Web API controller.
    [ApiController]
    public class SdgTargetController : ControllerBase{

        // Declaration of a private read-only field for accessing repository methods for data operations.
        private readonly ISdgTargetRepository _sdgTargetRepo; 
        private readonly ISdgTargetSubGoalRepository _subgoalRepo;
        private readonly ISdgTargetSdgTypeRepository _sdgTargetSdgTypeRepo; 
        private readonly ISdgTypeRepository _sdgTypeRepo; 

        

        // Constructor that expects an implementation of the repository to perform data access operations.
        public SdgTargetController(ISdgTargetRepository sdgTargetRepo, ISdgTargetSubGoalRepository subgoalRepo, ISdgTargetSdgTypeRepository sdgTargetSdgTypeRepo, ISdgTypeRepository sdgTypeRepo){
            // _commentRepo = commentRepo; 
            _sdgTypeRepo = sdgTypeRepo;
            _sdgTargetSdgTypeRepo = sdgTargetSdgTypeRepo;
            _sdgTargetRepo = sdgTargetRepo; 
            _subgoalRepo = subgoalRepo; 
        }
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] QueryObject query){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            // Retrieves SDG targets from the database asynchronously.
            var sdgTargets = await _sdgTargetRepo.GetAllAsync(query);  
            // Maps each SDG target data to a Data Transfer Object (DTO) using a mapper defined in api.Mappers.
            if(sdgTargets ==null){
              return NotFound(); 
            }
            var SdgTargetDtos = sdgTargets.Select(s => s.ToSdgTargetDto());
            // Returns a 200 OK response with the mapped SDG targets.
            return Ok(SdgTargetDtos); 
        }

        // HTTP GET method to retrieve a specific SDG Target by its ID.
        // :int in the curly brackets, validstes the variable if it is really a string (typechecking)
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById([FromRoute] int id){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            // Retrieves a single SDG target by ID from the repository.
            //
            var sdg = await _sdgTargetRepo.GetByIdAsync(id);

            if(sdg ==null){
              return NotFound(); 
            }
            // Returns a 200 OK response with the mapped SDG target.
            return Ok(sdg.ToSdgTargetDto()); 
        }
        // [HttpPost("{companyId:int}")]
        [HttpPost]
        //Specifies what data must be transferred during creation
        // sdgTargetDto as parameter in this controller specifies the json structure from the frontend
        public async Task<IActionResult> Create([FromBody]CreateSdgTargetDto sdgTargetDto){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            // This step is necessary as the DTO is a simplified or customized version of the data
            // that is suitable for data transfer, and needs to be converted to the database model.
            
            var sdgTargetModel = sdgTargetDto.ToSdgTargetFromCreate(); 
            // Adds the new SDG target model to the database asynchronously.
            // This involves saving the model to the database which is handled by the repository.
            await _sdgTargetRepo.CreateAsync(sdgTargetModel); 
        
            // checks whether subgoals have been sent
            if (sdgTargetDto.SubGoals != null && sdgTargetDto.SubGoals.Any())
            {
                foreach (CreateSdgTargetSubGoalDto subGoalDto in sdgTargetDto.SubGoals)
                {
                    var subgoalModel = subGoalDto.ToSdgTargetSubGoalFromCreate(sdgTargetModel.Id);
                    await _subgoalRepo.CreateAsync(subgoalModel);

                    sdgTargetModel.SubGoals.Add(subgoalModel);
                }
            }
            if (sdgTargetDto.SdgTypeIds != null && sdgTargetDto.SdgTypeIds.Any()){
                
                foreach (var sdgTypeId in sdgTargetDto.SdgTypeIds)
                {
                    // sdgType aus datenbank holen 
                    var sdgType = await _sdgTypeRepo.GetByIdAsync(sdgTypeId);
                    if (sdgType == null)
                        throw new Exception("SdgType not found");
                        
                    var sdgTargetSdgTypeModel = new SdgTargetSdgType
                    {
                        // hier kann was fehlen 
                        // SdgTargetId = sdgTargetModel.Id,
                        // SdgTypeId = sdgTypeId,
                        SdgTarget = sdgTargetModel,
                        SdgType = sdgType
                    };
                    await _sdgTargetSdgTypeRepo.CreateAsync(sdgTargetSdgTypeModel); 
                    
                    sdgTargetModel.SdgTargetSdgTypes.Add(sdgTargetSdgTypeModel); 

                    // var sdg = await _sdgTargetRepo.GetByIdAsync(id);
                }
                
            }

            // generates an HTTP response of type 201 Created, which contains two main components
            // generates a "Location"-Header with the URL to the new generated SdgTarget and a Dto 
            // Uses the 'GetById' action to generate the URI, ensuring that the client can directly access the new SDG target via its ID.
            // nameof(GetById) - calls the function GetById above 
            return CreatedAtAction(nameof(GetById), new { id = sdgTargetModel.Id}, sdgTargetModel.ToSdgTargetDto()); 
            




            
        }

        [HttpDelete]
        [Route("{id:int}")]

        public async Task<IActionResult> Delete([FromRoute] int id){
            if(!ModelState.IsValid){
                return BadRequest(ModelState);
            }

            var sdgTargetModel = await _sdgTargetRepo.DeleteAsync(id); 

            if(sdgTargetModel == null){
                return NotFound("SdgTarget does not exists");
            }

            return Ok(sdgTargetModel); 
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] UpdateSdgTargetRequestDto updateSdgTargetRequestDto){
            if(!ModelState.IsValid){
                return BadRequest(ModelState);
            }
            var sdgTarget = await _sdgTargetRepo.UpdateAsync(id, updateSdgTargetRequestDto.ToSdgTargetFromUpdate());

            if(sdgTarget == null){
                NotFound("sdgTarget not found");
            }
            return Ok(sdgTarget.ToSdgTargetDto()); 
        }

        // public async Task<IActionResult> Create([FromBody] CreateSdgDto sdgDto){
        //     if(!ModelState.IsValid)
        //         return BadRequest(ModelState);
        //     // Funktion aus SdgMapper 
        //     var sdgModel = sdgDto.
        //     // CreatedAtAction
        //     return CreatedAtAction(nameof(GetById), new{})
        // }


    }
}