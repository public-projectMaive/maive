using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Dtos.SdgTargetDtos;
using api.Dtos.SdgTargetSubGoalDtos;
using api.Interfaces;
using api.Mappers;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/sdgTargetSubGoal")]
    [ApiController]
    public class SdgTargetSubGoalController : ControllerBase
    {
        private readonly ISdgTargetSubGoalRepository _subgoalRepo; 
        private readonly ISdgTargetRepository _sdgTargetRepo;
    
        public SdgTargetSubGoalController(ISdgTargetSubGoalRepository subgoalRepo, ISdgTargetRepository sdgTargetRepo){
            _subgoalRepo = subgoalRepo;
            _sdgTargetRepo = sdgTargetRepo;
        } 
        [HttpGet]
        public async Task<IActionResult> GetAll(){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
                
            var subgoals = await _subgoalRepo.GetAllAsync(); 

            var subgoalDto = subgoals.Select(s=>s.ToSdgTargetSubGoalDto());

            return Ok(subgoalDto); 
        }
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById([FromRoute] int id){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            
            var subgoal = await _subgoalRepo.GetByIdAsync(id);

            if(subgoal == null){
                return NotFound(); 
            }
            return Ok(subgoal.ToSdgTargetSubGoalDto());
        }
        [HttpPost("{sdgTargetId:int}")]

        public async Task<IActionResult> Create ([FromRoute] int sdgTargetId, CreateSdgTargetSubGoalDto createSdgTargetSubGoalDto){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            if(!await _sdgTargetRepo.SdgTargetExists(sdgTargetId))
            {
                return BadRequest("SdgTarget does not exist"); 
            }
            var subgoalModel = createSdgTargetSubGoalDto.ToSdgTargetSubGoalFromCreate(sdgTargetId); 
            await _subgoalRepo.CreateAsync(subgoalModel);

            return CreatedAtAction(nameof(GetById), new {id = subgoalModel.Id}, subgoalModel.ToSdgTargetSubGoalDto());
            
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Update ([FromRoute] int id, [FromBody] UpdateSdgTargetSubGoalDto updateSdgTargetSubGoalDto){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            var subgoalModel = await _subgoalRepo.UpdateAsync(id, updateSdgTargetSubGoalDto.ToSdgTargetSubGoalFromUpdate()); 

            if(subgoalModel == null){
                NotFound("Comment not found");
            }
            return Ok(subgoalModel.ToSdgTargetSubGoalDto()); 
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            var subgoalModel = await _subgoalRepo.DeleteAsync(id); 

            if(subgoalModel == null){
                return NotFound("Comment does not exists"); 
            }

            return Ok(subgoalModel); 
        }
        
    }
}
