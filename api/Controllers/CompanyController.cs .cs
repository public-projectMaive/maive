using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Data;
using api.Dtos.Company;
using api.Helpers;
using api.Interfaces;
using api.Mappers;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    // Route attribute to specify the base URL path for all actions within this controller.
    [Route("api/company")]

    // Inherits from ControllerBase to gain access to useful methods for handling HTTP requests.
    public class CompanyController : ControllerBase
    {
        // field that holds a reference to the repository handling company data.
        private readonly ICompanyRepository _companyRepo; 
        
        public CompanyController(ICompanyRepository companyRepo){
            _companyRepo = companyRepo;
             
        }

        // HTTP GET method to retrieve all companies from the database.
        [HttpGet]
        public async Task<IActionResult> GetAll(){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            // Calls the repository to asynchronously get all company data from the database.
            var companies = await _companyRepo.GetAllAsync();
            // Maps each company entity to a Data Transfer Object (DTO) to abstract the database model from the API consumer.
            var companyDto = companies.Select(c => c.ToCompanyDto()); 
            // Sends the list of company DTOs back to the client as a 200 OK response.
            return Ok(companyDto);
        }
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetById(int id){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            // Attempts to retrieve a company item by its ID using the company repository.
            var companyModel = await _companyRepo.GetByIdAsync(id); 

            // If no company item is found with the provided ID, returns a NotFound response.
            if(companyModel == null){
                return NotFound(); 
            }
            // If a company item is found, converts it to a companyDto object and returns it with an Ok response.
            return Ok(companyModel.ToCompanyDto()); 
        }
        [HttpPost]
        //Specifies what data must be transferred during creation

        public async Task<IActionResult> Create([FromBody] CreateCompanyDto createCompanyDto){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            var companyModel = createCompanyDto.ToCompanyFromCreateDTO(); 

            await _companyRepo.CreateAsync(companyModel);

            return CreatedAtAction(nameof(GetById), new {id=companyModel.Id}, companyModel.ToCompanyDto());

        }

        [HttpPut]
        [Route("{id:int}")]

        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] UpdateCompanyRequestDto updateCompanyRequestDto){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            var companyModel = await _companyRepo.UpdateAsync(id, updateCompanyRequestDto); 
            if (companyModel == null){
                return NotFound(); 
            }
            return Ok(companyModel.ToCompanyDto()); 
        }


        [HttpDelete]
        [Route("{id:int}")]

        public async Task<IActionResult> Delete([FromRoute] int id){
            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            var companyModel = await _companyRepo.DeleteAsync(id);

            if(companyModel == null){
                return NotFound();
            }

            return NoContent(); 
        }
    }
}