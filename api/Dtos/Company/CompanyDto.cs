using api.Models;


namespace api.Dtos.Company
{
    public class CompanyDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty; 

        

        public string Street { get; set; } = string.Empty; 
        public string StreetNumber { get; set; } = string.Empty; 
        public int Postalcode { get; set; }
        public string City { get; set; } = string.Empty; 
        public string Latitude { get; set; } = string.Empty; 
        public string Longitude { get; set; } = string.Empty; 
        // wenn hier fehler einfach alt 
        public List<SdgTarget> SdgTargets { get; set; } = new List<SdgTarget>(); 
        public List<AppUser> AppUsers { get; set; } = new List<AppUser>(); 
    }
}