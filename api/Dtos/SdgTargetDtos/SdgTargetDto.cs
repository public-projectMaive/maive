using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Dtos.SdgTargetSubGoalDtos;

namespace api.Dtos.SdgTargetDtos
{
    public class SdgTargetDto
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public bool IsDone { get; set; }
        public List<string> SdgTypeColors { get; set; } = new List<string>();
        public List<SdgTargetSubGoalDto> SubGoals {get; set; } = new List<SdgTargetSubGoalDto>(); 
        
    }
}