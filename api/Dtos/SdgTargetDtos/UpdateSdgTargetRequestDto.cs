using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using api.Dtos.SdgTargetSubGoalDtos;

namespace api.Dtos.SdgTargetDtos
{
    public class UpdateSdgTargetRequestDto
    {
        [Required]
        [MinLength(5, ErrorMessage = "Title must be 5 characters")]
        [MaxLength(280, ErrorMessage = "Title cannot be over 280 characters")]
        public string Title {get; set; } = string.Empty;

        [Required]
        [MinLength(5, ErrorMessage = "Content must be 5 characters")]
        [MaxLength(280, ErrorMessage =  "Content cannot be over 280 characters")]
        public string Description { get; set; } = string.Empty; 

        [Required]
        public bool IsDone {get; set; }

        public List<CreateSdgTargetSubGoalDto> SubGoals {get; set; } = new List<CreateSdgTargetSubGoalDto>(); 
    }
}