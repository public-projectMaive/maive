// Define our Routing rules 


import {createRouter, createWebHistory} from "vue-router";
import HomeView from "../views/HomeView.vue";
import LoginView from "@/views/LoginView.vue";
import AccountView from "@/views/AccountView.vue"; 
import GrünesKraftwerkView from "@/views/GrünesKraftwerkView.vue";
import MaterialDatenbankView from "@/views/MaterialDatenbankView.vue";
import SDGView from "@/views/SDGView.vue";
import CompanyView from "@/views/CompanyView.vue";
import CreateSdgView from "@/views/CreateSdgView.vue"; 
import AdminDashboardView from '../views/Dashboards/AdminDashboardView.vue'; 
import store from '../store/index.js'; 


const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/", 
            name: "home",
            component: HomeView
        },
        {
            path: "/login",
            name: "login", 
            component: LoginView
        },
        {
            path: "/account",
            name: "account",
            component: AccountView
        },
        {
            path: "/kraftwerk",
            name: "GrünesKraftwerk",
            component: GrünesKraftwerkView
        },
        {
            path: "/materialdb",
            name: "MaterialDatenbank",
            component: MaterialDatenbankView
        },
        {
            path: "/sdgs",
            name: "SDGs",
            component: SDGView
        },
        {
            path: "/company",
            name: "Company",
            component: CompanyView
        },
        {
            path: "/createsdg",
            name: "CreateSdg",
            component: CreateSdgView
        },
        {
            path: "/admin-dashboard",
            name: "AdminDashboard",
            component: AdminDashboardView,
            meta:{ requiresAuth: true}
        },
        
        

    ]
})

// Function for verify the authentication
function isAuthenticated(){
    return !!localStorage.getItem('authToken'); 
}

// Navigation Guards
router.beforeEach((to, from, next) => {
    // some - ests whether at least one element in the array fulfills the condition defined by the provided function.
    // matched - is an array of route records that correspond to the route that the user 
    // is trying to reach. These are created by nesting routes. For example, if a route /parent/child is visited,
    // the array contains records for both /parent and /child.
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth) // Check if the route requires authentication 
    const isAuthenticated = store.state.auth.isAuthenticated; 
    console.log(requiresAuth);
    console.log(isAuthenticated);

    if (requiresAuth && !isAuthenticated) {
        next('/login');
      } else {
        next();
      }
});

export default router