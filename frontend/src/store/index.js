import { createStore } from 'vuex'; 
// it is recommented to use the Option Api here 

export default createStore({
    state: {
        auth: {
            isAuthenticated: false,
        }      
    },
    mutations: {
        setAuth(state, status){
            state.auth.isAuthenticated = status;
            // console.log(state.auth.isAuthenticated); 
        }
        
    }, 
    actions: {
        authenticate(context, isAuthenticated){
            context.commit('setAuth', isAuthenticated);            
        }
    }
}); 